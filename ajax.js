let cardy = document.getElementById('cardy');
let countryArray = [];
let filteredname = [];

function Card(country) {

    return `<div class="row my-2 mx-1 justify-content-center">
    <div class="col-12 col-md-6 ">
        <div class="row border shadow-sm py-2">
            <div class="col-md-5">
                <img class=" card-img-top" src=${country.flag} alt="Card image cap">
            </div>
            <div class="col-md-7 ">
                <h1 class="card-title">${country.name}</h1>
                <h8  >Currency: ${country. currencies[0].name} </h8>
                <p >Current time and date :${country.timezones}</p>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="https://www.google.co.in/maps/place/${country.name}/@33.8524176,63.2106559,6z/data=!3m1!4b1!4m5!3m4!1s0x38d16eb6f8ff026d:0xf3b5460dbe96da78!8m2!3d33.93911!4d67.709953" target="_blank" class="btn w-100">Show Map</a>
                    </div>
                    <div class="col-sm-6">
                        <a  class="w-100 btn  " href="ass4_2_.html?cname=${country.alpha3Code}">Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> `
}

function search(countries) {

    let input = document.getElementById('seary');
    input.addEventListener('keyup', (e) => {
        let str = '';
        cardy.innerHTML = '';
        searchValue = e.target.value.toLowerCase();
        const foundCountries = countries.filter((country) => country.name.toLowerCase().includes(searchValue));
        foundCountries.map((country) => {
            str += Card(country);
        })
        cardy.innerHTML = str;
    })
}
window.addEventListener('load', () => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://restcountries.eu/rest/v2/all', true);

    xhr.onload = function () {
        countryArray = JSON.parse(this.responseText);
        str = '';
        for (key in countryArray) {
            str += Card(countryArray[key]);
        }
        cardy.innerHTML = str;
        search(countryArray)
    }
    xhr.send();

})

